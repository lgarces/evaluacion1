<%-- 
    Document   : index
    Created on : 19-abr-2021, 12:34:42
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Formulario Solicitud Crédito Hipotecario</title>
	<script type="text/javascript" src="js/validaciones.js"></script>
	<link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
</head>
<body>
    <form name="frmInteres" id="frmInteres" action="InteresControlador" method="POST">
        <table border="0" cellspacing="1" cellpadding="2" width="650" align="center">
            <tr>
                <td align="center">
                    <fieldset><legend>Calculo Interés</legend>
                        <table align="center" cellpadding="2" cellspacing="1" border="0" widht="500">
                            <tr>
                                <td align="center" colspan="2">
                                    <img src="imagenes/interes.jpg" class="imagenCentral">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" width="200">
                                    Nombre Cliente:
                                </td>
                                <td>
                                    <input type="text" id="txtCliente" name="txtCliente" maxlength="30" class="textosCajas" style="width: 200px;" tabindex="1" onblur="this.value=this.value.toUpperCase();" onclick="fnLimpiaErrores();" placeholder="Nombre">
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Edad Cliente:
                                </td>
                                <td>
                                    <input type="text" id="txtEdad" name="txtEdad" maxlength="2" class="textosCajas" style="width: 30px;" tabindex="2" onclick="fnLimpiaErrores();" placeholder="18">
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Monto a Invertir:
                                </td>
                                <td>
                                    <input type="text" id="txtMonto" name="txtMonto" maxlength="6" class="textosCajas" style="width: 200px;" tabindex="3" onclick="fnLimpiaErrores();" placeholder="50000">
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Cantidad Años:
                                </td>
                                <td>
                                    <select name="lstAnos" id="lstAnos" class="textosSelect" style="width: 211px;" tabindex="4" onclick="fnLimpiaErrores();">
                                        <option value="">-- SELECCIONE --</option>
                                        <option value="1">1 Año</option>
                                        <option value="2">2 Años</option>
                                        <option value="3">3 Años</option>
                                        <option value="4">4 Años</option>
                                        <option value="5">5 Años</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <input type="button" name="btnValidar" id="btnValidar" value="Simular" class="botonAccion" tabindex="5" onclick="fnValidaDatos();">/
                                    <input type="button" name="btnLimpiar" id="btnLimpiar" value="Limpiar" class="botonCancelar" tabindex="6" onclick="fnLimpiar();">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" id="errores" class="textosNormal"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
